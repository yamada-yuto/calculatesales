package jp.alhinc.yuto_yamada.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {


	public static void main(String[] args) {
		try {
			Map<String, String> brm = new HashMap<String, String>();
			Map<String, Long> brm1 = new HashMap<String, Long>();
			BufferedReader br = null;

			try {
				File file = new File(args[0], "branch.lst");

				if (!file.exists()) {
					System.out.print("支店定義ファイルが存在しません");
					return;
				}
				br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine()) != null) {
					String[] str = line.split(",");
					if (str.length != 2 || !str[0].matches("^[0-9]{3}$")) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					brm.put(str[0], str[1]);
					brm1.put(str[0], 0L);
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				br.close();
			}

			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String str) {
					if (str.matches("^[0-9]{8}.rcd$")) {
						return true;
					} else {
						return false;
					}
				}
			};

			File[] list = new File(args[0]).listFiles(filter);
			List<Integer> number = new ArrayList<>();
			for (int l = 0; l < list.length; l++) {
				String fileName = list[l].getName();
				number.add(Integer.parseInt(fileName.substring(0, 8)));
			}
			Collections.sort(number);

			for (int k = 1; k < number.size(); k++) {
				if (number.get(k) != number.get(k - 1) + 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			};

			List<String> idCheck = new ArrayList<String>(brm.keySet());

			for (int i = 0; i < list.length; i++) {
					try {
						FileReader fr1 = new FileReader(list[i]);
						br = new BufferedReader(fr1);
						String line1 = br.readLine(); //1行目
						String line2 = br.readLine(); //2行目
						String line3 = br.readLine();
						 long sales = Long.valueOf(line2);
						if (!(idCheck.contains(line1))) {
							System.out.println(list[i].getName() + "の支店コードが不正です");
							return;
						}
						if (line3 != null) {
							System.out.println(list[i].getName() + "のフォーマットが不正です");
							return;
						}
						long oldSales = brm1.get(line1);
						long total = oldSales + sales;
						if (total > 9999999999L) {
							System.out.println("合計金額が10桁を超えました");
							return;
						}
						brm1.put(line1, total); //mapのvalueを上書き
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					} finally {
						br.close();
					}
				}
				FileWriter file1 = new FileWriter(new File(args[0], "branch.out"));
				PrintWriter pw = new PrintWriter(new BufferedWriter(file1));
				try {
					for (Entry<String, String> entry : brm.entrySet()) {
						pw.println(entry.getKey() + " ," + entry.getValue() + ", " + brm1.get(entry.getKey()));
					}
				} finally {
					pw.close();
				}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}
